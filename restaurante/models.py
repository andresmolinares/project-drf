from django.db import models


# Create your models here.

class Client(models.Model):
    name = models.CharField(max_length=45)
    lastname = models.CharField(max_length=45)
    email = models.EmailField(max_length=45)
    phone = models.CharField(max_length=45)
    address = models.CharField(max_length=45)
    observations = models.CharField(max_length=45, null=True, blank=True)

    def __str__(self):
        return self.name + ' ' + self.lastname


class Waiter(models.Model):
    name = models.CharField(max_length=45)
    first_lastname = models.CharField(max_length=45)
    second_lastname = models.CharField(max_length=45)

    def __str__(self):
        return self.name


class Product(models.Model):
    CHOICE_TYPE = (
        ('saucer', 'Saucer'),
        ('drink', 'Drink'),
    )
    name = models.CharField(max_length=45)
    type = models.CharField(max_length=10, choices=CHOICE_TYPE)
    amount = models.FloatField()

    def __str__(self):
        return self.name


class Table(models.Model):
    number_diners = models.IntegerField()
    location = models.CharField(max_length=45)

    def __str__(self):
        return self.location


class Order(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.IntegerField()

    def __str__(self):
        return self.product.name


class Invoice(models.Model):
    date_invoice = models.DateField(auto_now=True)
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    waiter = models.ForeignKey(Waiter, on_delete=models.CASCADE)
    table = models.ForeignKey(Table, on_delete=models.CASCADE)
    orders = models.ManyToManyField(Order)

