from django.shortcuts import render

# Create your views here.
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView, DeleteView
from restaurante.models import Client, Product, Waiter, Table, Order, Invoice
from restaurante.forms import ClientForm, ProductForm, WaiterForm, TableForm, OrderForm, InvoiceForm


class ListClientView(ListView):
    model = Client
    template_name = 'restaurante/client/index.html'
    context_object_name = 'clients'


class CreateClientView(CreateView):
    form_class = ClientForm
    template_name = 'restaurante/client/create_client.html'
    success_url = reverse_lazy('restaurant:clients')


class UpdateClientView(UpdateView):
    form_class = ClientForm
    model = Client
    template_name = 'restaurante/client/edit_client.html'
    success_url = reverse_lazy('restaurant:clients')


class DeleteClientView(DeleteView):
    model = Client
    template_name = 'restaurante/client/delete_client.html'
    success_url = reverse_lazy('restaurant:clients')


class ListProductView(ListView):
    model = Product
    template_name = 'restaurante/products/index.html'
    context_object_name = 'products'


class CreateProductView(CreateView):
    form_class = ProductForm
    template_name = 'restaurante/products/create_product.html'
    success_url = reverse_lazy('restaurant:products')


class UpdateProductView(UpdateView):
    form_class = ProductForm
    model = Product
    template_name = 'restaurante/products/edit_product.html'
    success_url = reverse_lazy('restaurant:products')


class DeleteProductView(DeleteView):
    model = Product
    template_name = 'restaurante/products/delete_product.html'
    success_url = reverse_lazy('restaurant:products')


class ListWaiterView(ListView):
    model = Waiter
    template_name = 'restaurante/waiter/index.html'
    context_object_name = 'waiters'


class CreateWaiterView(CreateView):
    form_class = WaiterForm
    template_name = 'restaurante/waiter/create_waiter.html'
    success_url = reverse_lazy('restaurant:waiters')


class UpdateWaiterView(UpdateView):
    form_class = WaiterForm
    model = Waiter
    template_name = 'restaurante/waiter/edit_waiter.html'
    success_url = reverse_lazy('restaurant:waiters')


class DeleteWaiterView(DeleteView):
    model = Waiter
    template_name = 'restaurante/waiter/delete_waiter.html'
    success_url = reverse_lazy('restaurant:waiters')


class ListTableView(ListView):
    model = Table
    template_name = 'restaurante/table/index.html'
    context_object_name = 'tables'


class CreateTableView(CreateView):
    form_class = TableForm
    template_name = 'restaurante/table/create_table.html'
    success_url = reverse_lazy('restaurant:tables')


class UpdateTableView(UpdateView):
    form_class = TableForm
    model = Table
    template_name = 'restaurante/table/edit_table.html'
    success_url = reverse_lazy('restaurant:tables')


class DeleteTableView(DeleteView):
    model = Table
    template_name = 'restaurante/table/delete_table.html'
    success_url = reverse_lazy('restaurant:tables')


class ListOrderView(ListView):
    model = Order
    template_name = 'restaurante/order/index.html'
    context_object_name = 'orders'


class CreateOrderView(CreateView):
    form_class = OrderForm
    template_name = 'restaurante/order/create_order.html'
    success_url = reverse_lazy('restaurant:orders')


class UpdateOrderView(UpdateView):
    form_class = OrderForm
    model = Order
    template_name = 'restaurante/order/edit_order.html'
    success_url = reverse_lazy('restaurant:orders')


class DeleteOrderView(DeleteView):
    model = Order
    template_name = 'restaurante/order/delete_order.html'
    success_url = reverse_lazy('restaurant:orders')


class ListInvoiceView(ListView):
    model = Invoice
    template_name = 'restaurante/invoice/index.html'
    context_object_name = 'invoices'


class CreateInvoiceView(CreateView):
    form_class = InvoiceForm
    template_name = 'restaurante/invoice/create_invoice.html'
    success_url = reverse_lazy('restaurant:invoices')


class UpdateInvoiceView(UpdateView):
    form_class = InvoiceForm
    model = Invoice
    template_name = 'restaurante/invoice/edit_invoice.html'
    success_url = reverse_lazy('restaurant:invoices')


class DeleteInvoiceView(DeleteView):
    model = Invoice
    template_name = 'restaurante/invoice/delete_invoice.html'
    success_url = reverse_lazy('restaurant:invoices')