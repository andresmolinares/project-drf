from django.contrib import admin
from restaurante.models import Client, Product, Order, Table, Invoice, Waiter


# Register your models here.

@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    list_display = ('name', 'lastname', 'phone', 'email')
    search_fields = ('name', 'lastname', 'phone', 'email')
    list_filter = ('email',)


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ('name', 'type', 'amount')
    search_fields = ('name', 'type')
    list_filter = ('type',)


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = ('product', 'quantity')
    search_fields = ('product', 'quantity')
    list_filter = ('product',)


@admin.register(Table)
class TableAdmin(admin.ModelAdmin):
    list_display = ('number_diners', 'location')
    search_fields = ('number_diners', 'location')
    list_filter = ('location',)


@admin.register(Invoice)
class InvoiceAdmin(admin.ModelAdmin):
    list_display = ('client', 'waiter', 'date_invoice')
    search_fields = ('client', 'waiter', 'date_invoice')
    list_filter = ('client', 'waiter', 'date_invoice',)


@admin.register(Waiter)
class WaiterAdmin(admin.ModelAdmin):
    list_display = ('name', 'first_lastname', 'second_lastname')
    search_fields = ('name', 'first_lastname', 'second_lastname')
    list_filter = ('name',)
