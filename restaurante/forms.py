from django import forms

from restaurante.models import Client, Product, Waiter, Table, Order, Invoice


class ClientForm(forms.ModelForm):
    class Meta:
        model = Client
        fields = ('name', 'lastname', 'email', 'phone', 'address', 'observations')

    name = forms.CharField(max_length=100,
                           widget=forms.TextInput(attrs={'class': 'form-control my-2', 'placeholder': 'Name'}))
    lastname = forms.CharField(max_length=100,
                               widget=forms.TextInput(attrs={'class': 'form-control my-2', 'placeholder': 'Lastname'}))
    email = forms.EmailField(widget=forms.EmailInput(attrs={'class': 'form-control my-2', 'placeholder': 'Email'}))
    phone = forms.CharField(max_length=100,
                            widget=forms.TextInput(attrs={'class': 'form-control my-2', 'placeholder': 'Phone'}))
    address = forms.CharField(max_length=100,
                              widget=forms.TextInput(attrs={'class': 'form-control my-2', 'placeholder': 'Address'}))
    observations = forms.CharField(max_length=100,
                                   widget=forms.TextInput(
                                       attrs={'class': 'form-control my-2', 'placeholder': 'Observations'}))


class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = ('name', 'type', 'amount')

    name = forms.CharField(max_length=100,
                           widget=forms.TextInput(attrs={'class': 'form-control my-2', 'placeholder': 'Name'}))
    type = forms.ChoiceField(choices=Product.CHOICE_TYPE,
                             widget=forms.Select(attrs={'class': 'form-control my-2'}))
    amount = forms.FloatField(widget=forms.NumberInput(attrs={'class': 'form-control my-2', 'placeholder': 'Amount'}))


class WaiterForm(forms.ModelForm):
    class Meta:
        model = Waiter
        fields = ('name', 'first_lastname', 'second_lastname')

    name = forms.CharField(max_length=100,
                           widget=forms.TextInput(attrs={'class': 'form-control my-2', 'placeholder': 'Name'}))
    first_lastname = forms.CharField(max_length=100,
                                     widget=forms.TextInput(
                                         attrs={'class': 'form-control my-2', 'placeholder': 'First Lastname'}))
    second_lastname = forms.CharField(max_length=100,
                                      widget=forms.TextInput(
                                          attrs={'class': 'form-control my-2', 'placeholder': 'Second Lastname'}))


class TableForm(forms.ModelForm):
    class Meta:
        model = Table
        fields = ('number_diners', 'location')

    number_diners = forms.IntegerField(
        widget=forms.NumberInput(attrs={'class': 'form-control my-2', 'placeholder': 'Number Diners'}))
    location = forms.CharField(max_length=100,
                               widget=forms.TextInput(attrs={'class': 'form-control my-2', 'placeholder': 'Location'}))


class OrderForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = ('product', 'quantity')

    product = forms.ModelChoiceField(queryset=Product.objects.all(),
                                     widget=forms.Select(attrs={'class': 'form-control my-2'}))
    quantity = forms.IntegerField(
        widget=forms.NumberInput(attrs={'class': 'form-control my-2', 'placeholder': 'Quantity'}))


class InvoiceForm(forms.ModelForm):
    class Meta:
        model = Invoice
        fields = ('client', 'waiter', 'table', 'orders')

    client = forms.ModelChoiceField(queryset=Client.objects.all(),
                                    widget=forms.Select(attrs={'class': 'form-control my-2'}))
    waiter = forms.ModelChoiceField(queryset=Waiter.objects.all(),
                                    widget=forms.Select(attrs={'class': 'form-control my-2'}))
    table = forms.ModelChoiceField(queryset=Table.objects.all(),
                                   widget=forms.Select(attrs={'class': 'form-control my-2'}))
    orders = forms.ModelMultipleChoiceField(queryset=Order.objects.all(),
                                            widget=forms.CheckboxSelectMultiple(attrs={'class': 'form-check my-2'}))
