from django.urls import path

from restaurante.views import *

app_name = 'restaurant'
urlpatterns = [
    path('clients/', ListClientView.as_view(), name='clients'),
    path('clients/create_client', CreateClientView.as_view(), name='create_client'),
    path('clients/edit_client/<int:pk>', UpdateClientView.as_view(), name='edit_client'),
    path('clients/delete_client/<int:pk>', DeleteClientView.as_view(), name='delete_client'),
    path('products/', ListProductView.as_view(), name='products'),
    path('products/create_product', CreateProductView.as_view(), name='create_product'),
    path('products/edit_product/<int:pk>', UpdateProductView.as_view(), name='edit_product'),
    path('products/delete_product/<int:pk>', DeleteProductView.as_view(), name='delete_product'),
    path('waiters/', ListWaiterView.as_view(), name='waiters'),
    path('waiters/create_waiter', CreateWaiterView.as_view(), name='create_waiter'),
    path('waiters/edit_waiter/<int:pk>', UpdateWaiterView.as_view(), name='edit_waiter'),
    path('waiters/delete_waiter/<int:pk>', DeleteWaiterView.as_view(), name='delete_waiter'),
    path('tables/', ListTableView.as_view(), name='tables'),
    path('tables/create_table', CreateTableView.as_view(), name='create_table'),
    path('tables/edit_table/<int:pk>', UpdateTableView.as_view(), name='edit_table'),
    path('tables/delete_table/<int:pk>', DeleteTableView.as_view(), name='delete_table'),
    path('orders/', ListOrderView.as_view(), name='orders'),
    path('orders/create_order', CreateOrderView.as_view(), name='create_order'),
    path('orders/edit_order/<int:pk>', UpdateOrderView.as_view(), name='edit_order'),
    path('orders/delete_order/<int:pk>', DeleteOrderView.as_view(), name='delete_order'),
    path('invoices/', ListInvoiceView.as_view(), name='invoices'),
    path('invoices/create_invoice', CreateInvoiceView.as_view(), name='create_invoice'),
    path('invoices/edit_invoice/<int:pk>', UpdateInvoiceView.as_view(), name='edit_invoice'),
    path('invoices/delete_invoice/<int:pk>', DeleteInvoiceView.as_view(), name='delete_invoice'),

]